/**
 * @file loggingmanager.h
 * Declaration file for class CLoggingManager
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date June 4, 2018
 *
 * @par Copyright:
 * 
 */

#ifndef LOGGINGMANAGER_H
#define LOGGINGMANAGER_H

#include <Poco/Logger.h>

#include "config/logconfig.h"

namespace pocoHttpServer
{

	namespace config
	{

		class CLogConfig;
	}

	namespace utils
	{

		class CLoggingManager
		{
		public:
			typedef boost::shared_ptr<CLoggingManager> pointer;

			virtual ~CLoggingManager();

			static pointer getLoggingManager();

			void configure(config::CLogConfig::pointer pLogConfig);

			Poco::Logger& getLogger(std::string sModule);

		private:
//			std::map<std::string, Poco::Logger& > _mLoggers;
			static CLoggingManager::pointer _pManager;

			CLoggingManager();
			CLoggingManager(const CLoggingManager& orig);

			void addModule(std::string sModule, Poco::Message::Priority prio);
		};

	}
}
#endif /* LOGGINGMANAGER_H */

