/*
 * There should be a license here...
 */

/**
 * @file loggable.h
 * Declaration file for class Loggable
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 10 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef LOGGABLE_H
#define LOGGABLE_H

#include <string>
#include <Poco/LogStream.h>

#include "loggingmanager.h"

namespace pocoHttpServer
{
	namespace utils
	{

		class Loggable
		{
		public:
			Loggable() : _sLogger(""), _logStream(CLoggingManager::getLoggingManager()->getLogger("")) { };
			Loggable(std::string sLogger) : _sLogger(sLogger), _logStream(Poco::Logger::get(_sLogger)) { };
			Loggable(const Loggable& orig) : _sLogger(orig._sLogger), _logStream(Poco::Logger::get(_sLogger)) { };
			virtual ~Loggable();
			
		protected:
			std::string _sLogger;
			Poco::LogStream _logStream;
		private:

		};

	}
}

#endif /* LOGGABLE_H */

