/**
 * @file main.cpp
 * @brief Application main file
 *
 *  @author		Joao Silva <jazzy.blog@gmail.com>
 */

#include <chrono>
#include <thread>
#include <iostream>

#include <Poco/Net/HTTPServer.h>
#include <Poco/LogStream.h>

#include "config/appconfig.h"
#include "utils/loggingmanager.h"
#include "requesthandler/myrequesthandlerfactory.h"
#include "exceptions/invalidconfigfile.h"
#include "httpserver.h"

using namespace std;
namespace rh=pocoHttpServer::requesthandler;


void waitForTerminationRequest(rh::MyRequestHandlerFactory* pRecHandler)
{
	while(!pRecHandler->stopped())
	{
		std::this_thread::sleep_for(1s);
	}
}


/**
 * App entrance point.
 */
int main(int argc, char** argv)
{

	pocoHttpServer::config::AppConfig appConfig;
		try
	{
		appConfig.parse(argc, argv);
	}
	catch (pocoHttpServer::exception::InvalidConfigFile &e)
	{
		std::cout << "Error parsing configuration: "
				<< e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch (std::exception &e)
	{
		std::cout << "Error parsing configuration: "
				<< e.what() << std::endl;
		return EXIT_FAILURE;
	}
	catch (...)
	{
		return EXIT_FAILURE;
	}
	
	if(appConfig.hasVersion())
	{
		std::cout << appConfig.version() << std::endl;
		return EXIT_SUCCESS;
	}
	if(appConfig.hasHelp())
	{
		std::cout << appConfig.version() << std::endl;
		std::cout << appConfig.helpText() << std::endl;
		return EXIT_SUCCESS;
	}
	
	pocoHttpServer::config::CLogConfig::pointer pLogConfig = appConfig.getLoggingConfig();
	pocoHttpServer::utils::CLoggingManager::pointer pLoggingMgr = 
			pocoHttpServer::utils::CLoggingManager::getLoggingManager();
	pLoggingMgr->configure(pLogConfig);
	
	Poco::LogStream lstr(pLoggingMgr->getLogger("main"));
	lstr.fatal() << appConfig.version() << std::endl;
	
	
	lstr.debug() << "Setting parameters" << std::endl;
	Poco::Net::HTTPServerParams* pParams = new Poco::Net::HTTPServerParams;
	pParams->setMaxQueued(50);
	pParams->setMaxThreads(2);
	
	
	// set-up a server socket 
	lstr.debug() << "Creating request handler factory" << std::endl;
	rh::MyRequestHandlerFactory* pFactory = new rh::MyRequestHandlerFactory();

	Poco::UInt16 port = appConfig.getServerPort();
	pocoHttpServer::httpServer srv(pFactory,
			port,
			pParams);

	lstr.debug() << "Configuring server" << std::endl;
	srv.configure(appConfig.getPluginCfg());
	srv.initialise();
	
	// start the HTTPServer 
	lstr.debug() << "Starting server" << std::endl;
	srv.start(); 
	
	waitForTerminationRequest(pFactory);
	
	lstr.debug() << "Received termination request" << std::endl;

	// Stop the HTTPServer 
	srv.stop();
	return 0;
}

