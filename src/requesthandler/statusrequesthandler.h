/*
 * There should be a license here...
 */

/**
 * @file statusrequesthandler.h
 * Declaration file for class StatusRequestHandler
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 19 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef STATUSREQUESTHANDLER_H
#define STATUSREQUESTHANDLER_H

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include "myrequesthandlerfactory.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{

		class StatusRequestHandler : public IRequestHandler
		{
		public:
			StatusRequestHandler() = delete;
			StatusRequestHandler(MyRequestHandlerFactory::Ptr pFactory);
			StatusRequestHandler(const StatusRequestHandler& orig);
			virtual ~StatusRequestHandler();

			void configure(const boost::property_tree::ptree& config) override;
			void initialize() override;
			std::string name() const override;

			void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) override;
			
			IRequestHandler* clone() const override;

		private:
			MyRequestHandlerFactory::Ptr _pFactory;

			std::string link(std::string sTitle, std::string sURL) const;
			void printRequestData(Poco::Net::HTTPServerRequest& request) const;

		};
	}
}
#endif /* STATUSREQUESTHANDLER_H */

