/**
 * @file requesthandlerstore.h
 * Declaration file for class CRequestHandlerStore
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date September 1, 2019
 *
 * @par Copyright:
 * 
 */

#ifndef REQUESTHANDLERSTORE_H
#define REQUESTHANDLERSTORE_H

#include <map>

#include <Poco/ClassLoader.h>
#include <Poco/Manifest.h>
#include <boost/noncopyable.hpp>

#include "irequesthandler.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{
		class IRequestHandler;

		typedef Poco::ClassLoader<pocoHttpServer::requesthandler::IRequestHandler> PluginLoader;
		typedef Poco::Manifest<pocoHttpServer::requesthandler::IRequestHandler> PluginManifest;

		class CRequestHandlerStore : boost::noncopyable, public pocoHttpServer::utils::Loggable
		{
		public:
			virtual ~CRequestHandlerStore();

			typedef boost::shared_ptr<CRequestHandlerStore> Ptr;

			static Ptr getNewStore();

			IRequestHandler* loadRequestHandlerPlugin(std::string sLibraryFile, std::string sReqHandlerId);

			IRequestHandler* getRequestHandler(std::string sReqHandlerId);
			
			IRequestHandler* loadEmbeddedRequestHandler(IRequestHandler* pReqHandler, std::string sReqHandlerId);
		private:

			CRequestHandlerStore(); // Disables default constructor

			/**
			 * Initialization
			 */
			void _init();

			std::map<std::string, IRequestHandler*> _requestHandlers;
			//				pugg::Kernel m_kernel;
		};
	}
}

#endif /* REQUESTHANDLERSTORE_H */

