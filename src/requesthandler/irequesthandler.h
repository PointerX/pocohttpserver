/**
 * @file irequesthandler.h
 * Declaration file
 *
 * @author João Silva <jazzy.blog@gmail.com>
 *
 * @date July 9, 2015
 *
 * @par Copyright:
 * 
 */

#ifndef IREQUESTHANDLER_H
#define IREQUESTHANDLER_H

#pragma once

#include <string>

#include <boost/property_tree/ptree.hpp>

#include <Poco/Net/HTTPRequestHandler.h>
#include <Poco/Net/HTTPServerRequest.h>
#include <Poco/Net/HTTPServerResponse.h>

#include "utils/loggable.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{

		/**
		 * @interface IRequestHandler
		 * Defines a request handler interface.
		 * A request handler is an entity that handles a request received by
		 * the HTTP server
		 */
		class IRequestHandler : public Poco::Net::HTTPRequestHandler, public pocoHttpServer::utils::Loggable
		{
		public:


			/**
			 * Smart pointer type to be used by clients.
			 */
			typedef boost::shared_ptr<IRequestHandler> pointer;

			IRequestHandler() = delete;

			IRequestHandler(std::string sModule) : pocoHttpServer::utils::Loggable(sModule)
			{
			}

			virtual ~IRequestHandler()
			{
			}

			/**
			 * Gets the plugin' s name, which identifies it.
			 * @return The plugin identification name
			 */
			virtual std::string name() const = 0;

			/**
			 * Configures the simulator plugin.
			 * It is invoked after the plugin is loaded.
			 * @param config the plugin's configuration
			 */
			virtual void configure(const boost::property_tree::ptree& config) = 0;

			/**
			 * Initializes the plugin. It is invoked after configure()
			 * and before any execute() invocation.
			 */
			virtual void initialize() = 0;

			/**
			 * Handles a request.
			 * @param request The request received.
			 * @param response The response to send to the client
			 */
			virtual void handleRequest(Poco::Net::HTTPServerRequest& request, Poco::Net::HTTPServerResponse& response) = 0;

			/**
			 * Creates a new object and returns a pointer to it.
			 * @return Cloned object 
			 */
			virtual IRequestHandler* clone() const = 0;

		};
	}
}

#endif /* IREQUESTHANDLER_H */

