/*
 * There should be a license here...
 */

/**
 * @file myrequesthandlerfactory.cpp
 * Declaration file for class MyRequestHandlerFactory
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 18 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#include <Poco/URI.h>

#include "myrequesthandlerfactory.h"
#include "requesthandlerstore.h"
#include "irequesthandler.h"
#include "requesthandler.h"
#include "statusrequesthandler.h"

using namespace pocoHttpServer::requesthandler;

MyRequestHandlerFactory::MyRequestHandlerFactory() :
utils::Loggable("reqFactory"),
_stop(false),
_requestsHandled(0)
{
}

MyRequestHandlerFactory::MyRequestHandlerFactory(const MyRequestHandlerFactory& orig) :
utils::Loggable("reqFactory"),
_stop(orig._stop),
_requestsHandled(orig._requestsHandled)
{
}

MyRequestHandlerFactory::~MyRequestHandlerFactory()
{
}

IRequestHandler* MyRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest& request)
{
	_logStream.debug() << "Request received on URI " << request.getURI() << std::endl;
	_logStream.debug() << "URI path: " << Poco::URI(request.getURI()).getPath() << std::endl;
	_requestsHandled++;
	CRequestHandlerStore::Ptr pStore = CRequestHandlerStore::getNewStore();
	IRequestHandler* res = pStore->getRequestHandler(request.getURI());
	if (res == NULL)
	{
		_logStream.information() << "Falling back to default request handler" << std::endl;
		res = pStore->getRequestHandler("default");
	}
	_logStream.information() << "Handling request via " << res->name() << std::endl;

	return res;
}

bool MyRequestHandlerFactory::stopped() const
{
	return _stop;
}

void MyRequestHandlerFactory::stop()
{
	_stop = true;
}

void MyRequestHandlerFactory::setRequestsHandled(unsigned int _requestsHandled)
{
	this->_requestsHandled = _requestsHandled;
}

unsigned int MyRequestHandlerFactory::getRequestsHandled() const
{
	return _requestsHandled;
}
