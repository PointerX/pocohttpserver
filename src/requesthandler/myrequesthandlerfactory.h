/*
 * There should be a license here...
 */

/**
 * @file myrequesthandlerfactory.h
 * Declaration file for class MyRequestHandlerFactory
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 18 de Fevereiro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef MYREQUESTHANDLERFACTORY_H
#define MYREQUESTHANDLERFACTORY_H

#include <Poco/Net/HTTPRequestHandlerFactory.h>
#include <Poco/Net/HTTPServerRequest.h>

#include "irequesthandler.h"
#include "utils/loggingmanager.h"

namespace pocoHttpServer
{
	namespace requesthandler
	{

		class MyRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory, public utils::Loggable
		{
		public:
			typedef Poco::SharedPtr<MyRequestHandlerFactory> Ptr;
			
			IRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request) override;

			MyRequestHandlerFactory();
			MyRequestHandlerFactory(const MyRequestHandlerFactory& orig);
			virtual ~MyRequestHandlerFactory();
			bool stopped() const;
			void stop();
			void setRequestsHandled(unsigned int _requestsHandled);
			unsigned int getRequestsHandled() const;

		private:
			bool _stop;
			unsigned int _requestsHandled;
		};
	}
}
#endif /* MYREQUESTHANDLERFACTORY_H */

