/*
 * There should be a license here...
 */

/* 
 * File:   dynlibloadingerror.h
 * Author: Joao Silva <jazzy.blog@gmail.com>
 *
 * Created on 27 de Setembro de 2019, 17:41
 */

#ifndef DYNLIBLOADINGERROR_H
#define DYNLIBLOADINGERROR_H

#include "dynlibexception.h"

namespace pocoHttpServer
{
	namespace exception
	{

		class DynamicLibraryLoadError : public DynamicLibraryException
		{
		public:

			DynamicLibraryLoadError(std::string sLibraryFile,
					std::string errorMsg) :
			DynamicLibraryException(sLibraryFile),
			m_sdlerror(errorMsg)
			{
			};

			DynamicLibraryLoadError(const DynamicLibraryLoadError& orig) :
			DynamicLibraryException(orig.libraryFile())
			{
			};

			virtual ~DynamicLibraryLoadError()
			{
			};

			virtual std::string what() const
			{
				return "Dynamic library load error: " + m_sdlerror + ". Library: "
						+ libraryFile();
			}

		private:
			std::string m_sdlerror;
		};

	}
}

#endif /* DYNLIBLOADINGERROR_H */

