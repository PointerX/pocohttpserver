/*
 * There should be a license here...
 */

/**
 * @file pluginconfig.cpp
 * Declaration file for class PluginConfig
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 16 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#include "pluginconfig.h"

using namespace pocoHttpServer::config;

// <editor-fold defaultstate="collapsed" desc="PluginConfig class implementation">

PluginConfig::PluginData::PluginData(std::string uri,
		std::string filename) :
_uri(uri),
_pluginFilename(filename)
{

}

PluginConfig::PluginData::PluginData(std::string uri,
		std::string filename,
		boost::property_tree::ptree config) :
_uri(uri),
_pluginFilename(filename),
_config(config)
{

}

PluginConfig::PluginData::PluginData(const PluginData& orig) :
_uri(orig._uri),
_pluginFilename(orig._pluginFilename),
_config(orig._config)
{

}

PluginConfig::PluginData::~PluginData()
{

}

void PluginConfig::PluginData::pluginFilename(std::string _pluginFilename)
{
	this->_pluginFilename = _pluginFilename;
}

std::string PluginConfig::PluginData::pluginFilename() const
{
	return _pluginFilename;
}

void PluginConfig::PluginData::uri(std::string _uri)
{
	this->_uri = _uri;
}

std::string PluginConfig::PluginData::uri() const
{
	return _uri;
}

boost::property_tree::ptree PluginConfig::PluginData::getConfig() const
{
	return _config;
}

// </editor-fold>

PluginConfig::PluginConfig() :
_bLoadDefaultHandlers(true),
_sPluginDir("plugins")
{
}

PluginConfig::PluginConfig(const PluginConfig& orig)
{
}

PluginConfig::~PluginConfig()
{
}

void PluginConfig::pluginDir(std::string _sPluginDir)
{
	this->_sPluginDir = _sPluginDir;
}

std::string PluginConfig::pluginDir() const
{
	return _sPluginDir;
}

void PluginConfig::loadDefaultHandlers(bool _bLoadDefaultHandlers)
{
	this->_bLoadDefaultHandlers = _bLoadDefaultHandlers;
}

bool PluginConfig::loadDefaultHandlers() const
{
	return _bLoadDefaultHandlers;
}

PluginConfig::PluginData_iterator PluginConfig::addPluginConfig(PluginData::pointer pPlugin)
{
	return this->_plugins.insert(_plugins.end(), pPlugin);
}
