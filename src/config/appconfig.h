/*
 * There should be a license here...
 */

/**
 * @file appconfig.h
 * Declaration file for class appconfig
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 10 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#ifndef APPCONFIG_H
#define APPCONFIG_H

#include <boost/noncopyable.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>

#include "utils/loggable.h"
#include "pluginconfig.h"

namespace pocoHttpServer
{
	namespace config
	{

		class AppConfig : public boost::noncopyable, public utils::Loggable
		{
		public:
			typedef boost::shared_ptr<AppConfig> pointer;
			
			AppConfig();

			virtual ~AppConfig();
			
			/**
			 * Parses all application's configurations
			 * @param argc
			 * @param argv
			 */
			void parse(int argc, char ** argv);

			/**
			 * Gets the help description
			 * @return 
			 */
			std::string helpText() const;

			/**
			 * Was the version option supplied?
			 * @return 
			 */
			bool hasVersion() const;

			/**
			 * Was the help option supplied?
			 * @return 
			 */
			bool hasHelp() const;

			static const std::string version();
			
			CLogConfig::pointer getLoggingConfig() const;
            unsigned int getServerPort() const;
            PluginConfig::pointer getPluginCfg() const;
			
		private:

			bool _bHasHelp;
			bool _bHasVersion;

			std::string _sHelpText;

			std::string _sConfigFile;

			boost::program_options::variables_map _optionsMap;
			boost::property_tree::ptree _xmlConfig;

			CLogConfig::pointer _pLoggingCfg;
			unsigned int _serverPort;
			PluginConfig::pointer _pPluginCfg;
			
			// <editor-fold defaultstate="collapsed" desc="Private methods">
			void parseConfigFile();
			void parseLoggingConfig(boost::property_tree::ptree logging);
			void parseServerConfig(boost::property_tree::ptree server);
			void parsePluginsConfig(boost::property_tree::ptree plugins);
			// </editor-fold>

		};

	}
}

#endif /* APPCONFIG_H */

