/*
 * There should be a license here...
 */

/**
 * @file appconfig.cpp
 * Declaration file for class appconfig
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 10 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <boost/property_tree/xml_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>

using boost::property_tree::ptree;

#include "appconfig.h"

using namespace pocoHttpServer::config;
namespace po = boost::program_options;

const char* OPT_HELP = "help,h";
const char* OPT_VERSION = "version,v";
const char* OPT_CONFIG_FILE = "config,c";

const char* OPT_HELP_LONG = "help";
const char* OPT_VERSION_LONG = "version";
const char* OPT_CONFIG_FILE_LONG = "config";

const char* OPT_HELP_DESC = "produce help message";
const char* OPT_VERSION_DESC = "version";
const char* OPT_CONFIG_FILE_DESC = "specifies the configuration file (default: config.xml)";

const char *DEFAULT_CONFIG_FILE = "config.xml";

AppConfig::AppConfig() : Loggable("config"),
_bHasHelp(false),
_bHasVersion(false),
_sHelpText(""),
_sConfigFile(DEFAULT_CONFIG_FILE),
_pLoggingCfg(new CLogConfig),
_serverPort(8080),
_pPluginCfg(new PluginConfig)
{
}

AppConfig::~AppConfig()
{
}

void AppConfig::parse(int argc, char** argv)
{
	po::options_description desc("Options");
	desc.add_options()
			(OPT_HELP, OPT_HELP_DESC)
			(OPT_VERSION, OPT_VERSION_DESC)
			(OPT_CONFIG_FILE, po::value<std::string>(), OPT_CONFIG_FILE_DESC)
			;

	po::store(po::parse_command_line(argc, argv, desc), _optionsMap);

	if (_optionsMap.count(OPT_HELP_LONG))
	{
		_bHasHelp = true;
		std::ostringstream oss;
		oss << desc;
		_sHelpText = oss.str();
	}

	if (_optionsMap.count(OPT_VERSION_LONG))
	{
		_bHasVersion = true;
	}
	if (_bHasHelp || _bHasVersion)
	{
		// Help or Version options means app is not meant to run, 
		// it is unnecessary to continue
		return;
	}


	if (_optionsMap.count(OPT_CONFIG_FILE_LONG))
	{
		_sConfigFile = _optionsMap[OPT_CONFIG_FILE_LONG].as<std::string>();
	}

	parseConfigFile();
}

bool AppConfig::hasHelp() const
{
	return _bHasHelp;
}

bool AppConfig::hasVersion() const
{
	return _bHasVersion;
}

std::string AppConfig::helpText() const
{
	return _sHelpText;
}

const std::string AppConfig::version()
{
	return VERSION;
}

CLogConfig::pointer AppConfig::getLoggingConfig() const
{
	return _pLoggingCfg;
}

unsigned int AppConfig::getServerPort() const
{
	return _serverPort;
}

PluginConfig::pointer AppConfig::getPluginCfg() const
{
	return _pPluginCfg;
}

void AppConfig::parseConfigFile()
{
	// Load the XML file into the property tree. If reading fails
	// (cannot open file, parse error), an exception is thrown.
	try
	{
		read_xml(_sConfigFile, _xmlConfig, boost::property_tree::xml_parser::trim_whitespace);
	}
	catch (boost::property_tree::xml_parser_error &e)
	{
		throw e;
	}

	parseLoggingConfig(_xmlConfig.get_child("httpserver.logging"));
	parseServerConfig(_xmlConfig.get_child("httpserver.server"));
	parsePluginsConfig(_xmlConfig.get_child("httpserver.plugins"));
}

void AppConfig::parseLoggingConfig(boost::property_tree::ptree logging)
{

	BOOST_FOREACH(ptree::value_type const& module, logging.get_child("modules"))
	{
		if (module.first == "module")
		{
			std::string sModule = module.second.get<std::string>("<xmlattr>.id");
			std::string sLevel = module.second.get_value<std::string>();
			boost::algorithm::trim(sLevel);
			boost::algorithm::to_upper(sLevel);

			Poco::Message::Priority loggingPriority;
			if (sLevel == "FATAL")
			{
				loggingPriority = Poco::Message::PRIO_FATAL;
			}
			else if (sLevel == "CRITICAL")
			{
				loggingPriority = Poco::Message::PRIO_CRITICAL;
			}
			else if (sLevel == "ERROR")
			{
				loggingPriority = Poco::Message::PRIO_ERROR;
			}
			else if (sLevel == "WARNING")
			{
				loggingPriority = Poco::Message::PRIO_WARNING;
			}
			else if (sLevel == "NOTICE")
			{
				loggingPriority = Poco::Message::PRIO_NOTICE;
			}
			else if (sLevel == "INFORMATION")
			{
				loggingPriority = Poco::Message::PRIO_INFORMATION;
			}
			else if (sLevel == "DEBUG")
			{
				loggingPriority = Poco::Message::PRIO_DEBUG;
			}
			else if (sLevel == "TRACE")
			{
				loggingPriority = Poco::Message::PRIO_TRACE;
			}
			else
			{
				// Unknown logging level, ignore it
				continue;
			}
			_pLoggingCfg->addLogModule(sModule, loggingPriority);
		}
	}
	try
	{
		std::string sOutputType = logging.get<std::string>("output.<xmlattr>.type");
		if (sOutputType == "console")
		{
			_pLoggingCfg->setLogType(CLogConfig::OT_Console);
		}
		else if (sOutputType == "file")
		{
			_pLoggingCfg->setLogType(CLogConfig::OT_File);
			std::string sFileName = logging.get<std::string>("output.<xmlattr>.path");
			std::string sRotation = logging.get<std::string>("output.<xmlattr>.rotation");
			_pLoggingCfg->setFileLog(sFileName, sRotation);
		}
	}
	catch (boost::property_tree::ptree_error &e)
	{
		_pLoggingCfg->setLogType(CLogConfig::OT_Console);
	}

}

void AppConfig::parseServerConfig(boost::property_tree::ptree server)
{
	_serverPort = server.get<unsigned int>("port");
}

void AppConfig::parsePluginsConfig(boost::property_tree::ptree plugins)
{
//	{
//		boost::property_tree::xml_writer_settings<std::string> settings(' ', 4);
//		boost::property_tree::write_xml(std::cout, plugin.second, settings);
//	}
	_pPluginCfg->loadDefaultHandlers("true" == plugins.get<std::string>("<xmlattr>.loadDefault"));
	_pPluginCfg->pluginDir(plugins.get<std::string>("<xmlattr>.pluginDir"));

	for (auto plugin : plugins.get_child(""))
	{
		if (plugin.first != "plugin")
		{
			continue;
		}
		bool bEnabled = plugin.second.get<bool>("<xmlattr>.enabled");
		if (!bEnabled)
		{
			continue;
		}

		std::string sUri = plugin.second.get<std::string>("<xmlattr>.uri");
		std::string sFilename = plugin.second.get<std::string>("<xmlattr>.file");

		boost::property_tree::ptree pluginCfg;
		if (plugin.second.find("config") != plugin.second.not_found())
		{
			pluginCfg = plugin.second.get_child("");
		}
		_pPluginCfg->addPluginConfig(
				PluginConfig::PluginData::pointer(
				new PluginConfig::PluginData(sUri, sFilename, pluginCfg)));
	}
}
