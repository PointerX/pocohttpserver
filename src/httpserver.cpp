/*
 * There should be a license here...
 */

/**
 * @file httpserver.cpp
 * Declaration file for class httpServer
 *
 * @author Joao Silva <jazzy.blog@gmail.com>
 *
 * @date 3 de Setembro de 2019
 *
 * @par Copyright:
 * 
 */

#include <cstddef>
#include <boost/foreach.hpp>

#include "httpserver.h"
#include "requesthandler/myrequesthandlerfactory.h"
#include "requesthandler/requesthandler.h"
#include "requesthandler/statusrequesthandler.h"
#include "exceptions/dynlibexception.h"

using namespace pocoHttpServer;
namespace rh = pocoHttpServer::requesthandler;

httpServer::httpServer(requesthandler::MyRequestHandlerFactory::Ptr pFactory,
		Poco::UInt16 nHttpPort,
		Poco::Net::HTTPServerParams::Ptr pServerParams) :
utils::Loggable("server"),
_pRequestFactory(pFactory),
_pRequestHandlerStore(rh::CRequestHandlerStore::getNewStore()),
_srv(pFactory, nHttpPort, pServerParams),
_stoped(true)
{
}

httpServer::~httpServer()
{
}

void httpServer::configure(config::PluginConfig::pointer pPluginCfg)
{
	if (pPluginCfg->loadDefaultHandlers())
	{
		_logStream.warning() << "Adding default request handlers" << std::endl;
		_pRequestHandlerStore->loadEmbeddedRequestHandler(new pocoHttpServer::requesthandler::RequestHandler(_pRequestFactory), "default");
		_pRequestHandlerStore->loadEmbeddedRequestHandler(new pocoHttpServer::requesthandler::StatusRequestHandler(_pRequestFactory), "/status");
	}
	_logStream.information()
			<< "Will configure "
			<< pPluginCfg->getNrOfConfiguredPlugins()
			<< " plugins"
			<< std::endl;

	std::for_each(pPluginCfg->getPluginsDataBegin(),
			pPluginCfg->getPluginsDataEnd(),
			[ = ] (config::PluginConfig::PluginData::pointer currentPlugin){
		_logStream.debug() << "Loading plugin from " << currentPlugin->pluginFilename() << std::endl;
		try
		{
			auto pPlugin = loadRequestHandler(pPluginCfg->pluginDir() + "/" + currentPlugin->pluginFilename(),
					currentPlugin->uri());
			pPlugin->configure(currentPlugin->getConfig());
			pPlugin->initialize();
		}
		catch (exception::DynamicLibraryException &e)
		{
			_logStream.warning()
					<< "Failed to load request handler for URI "
					<< currentPlugin->uri()
					<< " from library "
					<< e.libraryFile()
					<< ". " << e.what()
					<< std::endl;
		}
	});
}

requesthandler::IRequestHandler* httpServer::loadRequestHandler(const std::string sLibraryFile,
		const std::string sUri)
{
	requesthandler::IRequestHandler* pRes = NULL;
	return _pRequestHandlerStore->loadRequestHandlerPlugin(
			sLibraryFile,
			sUri);
}

void httpServer::initialise()
{
}

void httpServer::start()
{
	_logStream.information()
			<< "Starting server on port "
			<< _srv.port()
			<< std::endl;
	_srv.start();
	_stoped = false;
}

void httpServer::stop()
{
	_srv.stop();
	_stoped = true;
}

bool httpServer::stoped()
{
	return _stoped;
}
